<div class="body-inner">
  <div class="container">

    <?php get_template_part('templates/page', 'header'); ?>

    <?php if (!have_posts()) : ?>
      <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'sage'); ?>
      </div>
      <?php get_search_form(); ?>
    <?php endif; ?>

    <div class="row posts-list posts-list_blog">

      <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/content', 'loop'); ?>
      <?php endwhile; ?>
    </div><!-- row -->

    <?php the_posts_navigation(); ?>

  </div>

</div>
