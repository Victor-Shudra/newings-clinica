<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'footer_navigation' => __('Footer Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  
  
  // logo
  add_image_size('w150h120', 150, 120, false);
  
  // banners
  add_image_size('h672', 0, 672, false);
  
  // team
  add_image_size('w185h230', 185, 230, false);
  
  // about
  add_image_size('w421', 421, 0, false);
  
  // services
  add_image_size('s100', 100, 100, false);
  
  // advice
  add_image_size('s350', 350, 350, false);
  
  // testimonials
  add_image_size('s200', 200, 200, false);
  
  
//  
//  // logo scrolled
//  add_image_size('w84h83', 84, 83, false);
//  
//  // logo footer
//  add_image_size('w134h128', 134, 128, false);
//  
//  
//  // who we are
//  add_image_size('w423h415', 423, 415, false);
//  
//  
//  // map 1
//   add_image_size('w131h51', 131, 51, false);
//  
//  // map 2
//   add_image_size('w61h61', 61, 61, false);
//   
//   
//   // news home
//  add_image_size('w421h416', 421, 416, false);
//  add_image_size('w226h143', 226, 143, ['center', 'center']);
  
  
  
  
//   add_image_size('h801', 0, 801, false);
//   add_image_size('h480', 0, 480, false);
//   add_image_size('h484', 0, 484, false);

  

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
//  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
//  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
//  register_sidebar([
//    'name'          => __('Primary', 'sage'),
//    'id'            => 'sidebar-primary',
//    'before_widget' => '<section class="widget %1$s %2$s">',
//    'after_widget'  => '</section>',
//    'before_title'  => '<h3>',
//    'after_title'   => '</h3>'
//  ]);
//
//  register_sidebar([
//    'name'          => __('Footer', 'sage'),
//    'id'            => 'sidebar-footer',
//    'before_widget' => '<section class="widget %1$s %2$s">',
//    'after_widget'  => '</section>',
//    'before_title'  => '<h3>',
//    'after_title'   => '</h3>'
//  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
