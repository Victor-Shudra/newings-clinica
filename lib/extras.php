<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

show_admin_bar(0);

// Theme Settings page
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title' => 'Theme Settings',
    'menu_title' => 'Theme Settings',
    'menu_slug' => 'theme-settings',
    'capability' => 'edit_posts',
    'redirect' => false
  ));
}


function cptui_register_my_cpts() {

	/**
	 * Post Type: Team members.
	 */

	$labels = array(
		"name" => __( 'Team members', 'sage' ),
		"singular_name" => __( 'Team member', 'sage' ),
	);

	$args = array(
		"label" => __( 'Team members', 'sage' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "team", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "team", $args );

	/**
	 * Post Type: Services.
	 */

	$labels = array(
		"name" => __( 'Services', 'sage' ),
		"singular_name" => __( 'Service', 'sage' ),
	);

	$args = array(
		"label" => __( 'Services', 'sage' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "services", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "services", $args );

	/**
	 * Post Type: Advices.
	 */

	$labels = array(
		"name" => __( 'Advices', 'sage' ),
		"singular_name" => __( 'Advice', 'sage' ),
	);

	$args = array(
		"label" => __( 'Advices', 'sage' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "advices", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "advices", $args );

	/**
	 * Post Type: Testimonials.
	 */

	$labels = array(
		"name" => __( 'Testimonials', 'sage' ),
		"singular_name" => __( 'Testimonial', 'sage' ),
	);

	$args = array(
		"label" => __( 'Testimonials', 'sage' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "testimonials", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "testimonials", $args );
}

add_action( 'init', __NAMESPACE__ . '\\cptui_register_my_cpts' );





//add_filter('gform_submit_button', __NAMESPACE__ . '\\form_submit_button', 10, 2);
//
//function form_submit_button($button, $form) {
//  return "<div class='w-submit'><button class='wpcf7-form-control wpcf7-submit btn_submit' id='gform_submit_button_{$form['id']}'><span>submit form</span></button></div>";
//}

// clear dashboard
function remove_menus() {
    remove_menu_page('edit-comments.php');          // Comments
    remove_menu_page('edit.php');      // Posts
}

add_action('admin_menu', __NAMESPACE__ . '\\remove_menus');

 


// clear wp admin bar
function remove_wp_nodes() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_node('new-post');
    $wp_admin_bar->remove_node('comments');
}

add_action('admin_bar_menu', __NAMESPACE__ . '\\remove_wp_nodes', 999); 


function cc_mime_types( $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', __NAMESPACE__ . '\\cc_mime_types' ); 




// functions.php
// Get terms for all custom taxonomies
// get taxonomies terms links
function custom_taxonomies_terms_links(){
  // get post by post id
  $post = get_post( $post->ID );

  // get post type by post
  $post_type = $post->post_type;

  // get post type taxonomies
  $taxonomies = get_object_taxonomies( $post_type, 'objects' );

  $out = array();
  foreach ( $taxonomies as $taxonomy_slug => $taxonomy ){

    // get the terms related to post
    $terms = get_the_terms( $post->ID, $taxonomy_slug );

    if ( !empty( $terms ) ) {
      $out[] = " - ";
      foreach ( $terms as $term ) {
        $out[] =
          '  <a href="'
        .    get_term_link( $term->slug, $taxonomy_slug ) .'">'
        .    $term->name
        . "</a>";
      }
      $out[] = "";
    }
  }

  return implode('', $out );
}


//

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

//  if(is_front_page()) {
//    $classes[] = 'sidebar-primary';
//  }

  return $classes;
}

add_filter('body_class', __NAMESPACE__ . '\\body_class');

// sitemap shortcode
// [sitemap]
function sitemap_func($atts, $content = null) {
  ob_start();
  wp_nav_menu([
    'theme_location' => 'sitemap_navigation',
    'menu_class' => ''
  ]);

  $output = ob_get_contents();
  ob_end_clean();

  return $output;
}

add_shortcode('sitemap', __NAMESPACE__ . '\\sitemap_func');

// button shortcode
// [button link="" target=""]text[/button]
function button_func($atts, $content = null) {


  $attr = shortcode_atts(array(
    'link' => NULL,
    'target' => NULL,
      ), $atts);

  if ($attr['target']):
    $source = '<a href="' . $attr['link'] . '" target="_blank" class="btn_blue_sh">' . $content . '</a>';
  else:
    $source = '<a href="' . $attr['link'] . '" class="btn_blue_sh">' . $content . '</a>';

  endif;
  return $source;
}

add_shortcode('button', __NAMESPACE__ . '\\button_func');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

function breadcrumb_trail($args = array()) {
  global $wp_query;

  /* Set up the default arguments for the breadcrumb. */
  $defaults = array(
    'separator' => '/',
    'before' => '',
    'after' => false,
    'front_page' => true,
    'show_home' => __('Home', 'am'),
    'single_tax' => 'category',
    'format' => 'flat', // Implement later
    'echo' => true
  );

  /* Apply filters to the arguments. */
  $args = apply_filters('breadcrumb_trail_args', $args);

  /* Parse the arguments and extract them for easy variable naming. */
  extract(wp_parse_args($args, $defaults));

  if ($separator)
    $separator = '<span class="sep">' . $separator . '</span>';

  if (is_front_page() && !$front_page)
    return apply_filters('breadcrumb_trail', false);

  if ($show_home && is_front_page())
    $trail['trail_end'] = "{$show_home}";
  elseif ($show_home)
    $trail[]            = '<a href="' . esc_url(home_url() . '/') . '" title="' . esc_attr(get_bloginfo('name')) . '" rel="home" class="trail-begin">' . $show_home . '</a>';

  if (is_home() && !is_front_page()) {
    $home_page = get_page($wp_query->get_queried_object_id());

    $parent_id = $home_page->post_parent;
    while ($parent_id) {
      $page      = get_page($parent_id);
      $parents[] = '<a href="' . get_permalink($page->ID) . '" title="' . esc_attr(get_the_title($page->ID)) . '">' . get_the_title($page->ID) . '</a>';
      $parent_id = $page->post_parent;
    }
    if ($parents) {
      $parents = array_reverse($parents);
      foreach ($parents as $parent)
        $trail[] = $parent;
    }
    $trail['trail_end'] = get_the_title($home_page->ID);
  } elseif (is_singular()) {

    if (is_page()) {
      $parent_id = $wp_query->post->post_parent;
      while ($parent_id) {
        $page      = get_page($parent_id);
        $parents[] = '<a href="' . get_permalink($page->ID) . '" title="' . esc_attr(get_the_title($page->ID)) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id = $page->post_parent;
      }
      if (isset($parents)) {
        $parents = array_reverse($parents);
        foreach ($parents as $parent)
          $trail[] = $parent;
      }
    } elseif (is_attachment()) {
      $trail[] = '<a href="' . get_permalink($wp_query->post->post_parent) . '" title="' . esc_attr(get_the_title($wp_query->post->post_parent)) . '">' . get_the_title($wp_query->post->post_parent) . '</a>';
    } elseif (is_single()) {
      if ($single_tax && $terms   = get_the_term_list($wp_query->post->ID, $single_tax, '', '<span class="sep_tax">, </span>', ''))
        $trail[] = $terms;
    }

    $trail['trail_end'] = get_the_title();
  }

  elseif (is_archive()) {

    if (is_tax() || is_category() || is_tag()) {
      $term = $wp_query->get_queried_object();

      if (!isset($trail['trail_end']))
        $trail['trail_end'] = '';

      if (is_category() && $term->parent) {
        $parents            = get_category_parents($term->parent, true, " {$separator} ", false);
        if ($parents)
          $trail['trail_end'] = $parents;
      }

      $trail['trail_end'] .= $term->name;
    }

    elseif (is_author())
      $trail['trail_end'] = get_the_author_meta('display_name', get_query_var('author'));

    elseif (is_time()) {

      if (get_query_var('minute') && get_query_var('hour'))
        $trail['trail_end'] = get_the_time(__('g:i a', 'am'));

      elseif (get_query_var('minute'))
        $trail['trail_end'] = sprintf(__('Minute %1$s', 'am'), get_the_time(__('i', 'am')));

      elseif (get_query_var('hour'))
        $trail['trail_end'] = get_the_time(__('g a', 'am'));
    }

    elseif (is_date()) {

      if (is_day()) {
        $trail[]            = '<a href="' . get_year_link(get_the_time(__('Y', 'am'))) . '" title="' . esc_attr(get_the_time(__('Y', 'am'))) . '">' . get_the_time(__('Y', 'am')) . '</a>';
        $trail[]            = '<a href="' . get_month_link(get_the_time(__('Y', 'am')), get_the_time(__('m', 'am'))) . '" title="' . esc_attr(get_the_time(__('F', 'am'))) . '">' . get_the_time(__('F', 'am')) . '</a>';
        $trail['trail_end'] = get_the_time(__('j', 'am'));
      } elseif (get_query_var('w')) {
        $trail[]            = '<a href="' . get_year_link(get_the_time(__('Y', 'am'))) . '" title="' . esc_attr(get_the_time(__('Y', 'am'))) . '">' . get_the_time(__('Y', 'am')) . '</a>';
        $trail['trail_end'] = sprintf(__('Week %1$s', 'hybrid'), get_the_time(__('W', 'am')));
      } elseif (is_month()) {
        $trail[]            = '<a href="' . get_year_link(get_the_time(__('Y', 'am'))) . '" title="' . esc_attr(get_the_time(__('Y', 'am'))) . '">' . get_the_time(__('Y', 'am')) . '</a>';
        $trail['trail_end'] = get_the_time(__('F', 'am'));
      } elseif (is_year()) {
        $trail['trail_end'] = get_the_time(__('Y', 'am'));
      }
    }
  } elseif (is_search())
    $trail['trail_end'] = sprintf(__('Search results for &quot;%1$s&quot;', 'am'), esc_attr(get_search_query()));

  elseif (is_404())
    $trail['trail_end'] = __('404 Not Found', 'am');

  /* Connect the breadcrumb trail. */
  $breadcrumb = '';
  $breadcrumb .= " {$before} ";
  if (is_array($trail))
    $breadcrumb .= join(" {$separator} ", $trail);
  $breadcrumb .= '';

  $breadcrumb = apply_filters('breadcrumb_trail', $breadcrumb);

  /* Output the breadcrumb. */
  if ($echo)
    echo $breadcrumb;
  else
    return $breadcrumb;
}


// function bc_custom_js() {
// ?>

<?php
// }
// add_action( 'wp_footer', 'bc_custom_js' );