<?php

use Roots\Sage\Extras;
?>

<div class="body-inner page-404">
  <div class="container">


    <div class="bc">
      <div class="row">
        <div class="col-md-12 text-left">
          <?php Extras\breadcrumb_trail('echo=1&separator=|'); ?>
        </div><!-- col-md-12 -->
      </div><!-- row -->
    </div><!-- bc -->


    <div class="row">
      <div class="col-md-12">

        <div class="tour">
          <h1 class="tour__page-title">Sorry, that page<br>doesn't exist!</h1>
          <div class="tour__page-subtitle">Thanks for noticing - we're going to fix it up and have things back to normal soon.</div>
          <form role="search" method="get" class="search-form"
                action="<?php bloginfo('url') ?>">
            <label>
              <span class="screen-reader-text">Search for:</span>
              <input type="search" class="search-field" placeholder="Search …" value="" name="s">
            </label>
            <div class="text-center">

              <button type="submit" class="search-submit">Search</button>
            </div><!-- text-center -->
          </form>
        </div>


      </div>
    </div>


  </div>
</div>