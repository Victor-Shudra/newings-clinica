<?php

use Roots\Sage\Extras; ?>
<?php while (have_posts()) : the_post(); ?>


  <div class="body-inner">
    <div class="container">


      <div class="bc">
        <div class="row">
          <div class="col-md-12">
            <?php Extras\breadcrumb_trail('echo=1&separator=|'); ?>
          </div><!-- col-md-12 -->
        </div><!-- row -->
      </div><!-- bc -->


      <?php
      $styled_title = get_field('styled_title');
      $subtitle     = get_field('subtitle');
      ?>
      <!--<div class="inner-page">-->
      <div class="row">
        <div class="col-md-12">

          <div class="header-intro header-intro_sub">

            <?php if ($styled_title): ?>
              <h1><?php echo $styled_title ?></h1>
            <?php else: ?>
              <h1><?php the_title(); ?></h1>
            <?php endif; ?>

            <?php if ($subtitle): ?>
              <h3><?php echo $subtitle ?></h3>
            <?php endif; ?>

          </div><!-- header-intro -->
        </div>
      </div><!-- row -->



      <div class="row row_page_inner">

        
          <?php
          //Getting page first parent
          $parent          = array_reverse(get_post_ancestors($post->ID));
          $first_parent    = get_page($parent[0]);
          $first_parent_ID = $first_parent->ID;

          //Adding sidebar navigation
          //if 1st level page has children
          $children = get_pages('child_of=' . $first_parent_ID);
          if (count($children) != 0):
            ?>
        
        <div class="col-md-2">

            <div class="sidebar-menu">
              <h2><a href="<?php echo get_page_link($first_parent_ID) ?>"><?php echo get_the_title($first_parent_ID) ?></a></h2>

              <ul>

                <?php
                $args = array(
                  'child_of' => $first_parent_ID,
                  'echo' => 1,
                  'post_type' => 'page',
                  'post_status' => 'publish',
                  'sort_column' => 'menu_order',
                  'title_li' => __(''),
                  'walker' => new Walker_Page
                );
                //Page list output
                wp_list_pages($args);
                ?>

              </ul>

            </div>


 </div><!-- col-md-2 -->
 <div class="col-md-4 col-md-push-6">
       
          <?php else: ?>
        <div class="col-md-4 col-md-push-8">
          <?php endif; ?> 
          <?php
          $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w338h269', false);

          $icon = get_field('icon');
          ?>
          <?php if ($image_url): ?>
            <div class="w-thumb-page">
              <?php if ($icon): ?>
                <div class="icon">
                  <img src="<?php echo $icon['sizes']['w30h28'] ?>" alt="">
                </div><!-- icon -->
              <?php endif; ?>

              <img src="<?php echo $image_url[0]; ?>" alt=""> 
            </div><!-- w-thumb-page -->
          <?php endif; ?>
        </div><!-- col-md-3 -->
        
        
          <?php if($image_url || $icon): ?>
<div class="col-md-6 col-md-pull-4">
  
      <?php else: ?>
  <div class="col-md-12">
          <?php endif; ?>
        
          <div class="page_content__inner">

            <?php the_content(); ?>
          </div><!-- page_content__inner -->
        </div><!-- col-md-7 -->



        
      </div><!-- row -->



    </div><!-- container -->
  </div><!-- body -->


<?php endwhile; ?>
