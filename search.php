<?php

use Roots\Sage\Extras;
?>

<div class="body-inner">
  <div class="container">


    <div class="bc">
      <div class="row">
        <div class="col-md-12 text-left">
          <?php Extras\breadcrumb_trail('echo=1&separator=|'); ?>
        </div><!-- col-md-12 -->
      </div><!-- row -->
    </div><!-- bc -->


    <div class="row row_page_inner">
      <div class="col-md-12">
        <?php get_template_part('templates/page', 'header'); ?>

        <?php if (!have_posts()) : ?>
          <div class="alert alert-warning">
            <?php _e('Sorry, no results were found.', 'sage'); ?>
          </div>
          <?php get_search_form(); ?>
        <?php endif; ?>

        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', 'search'); ?>
        <?php endwhile; ?>

        <?php the_posts_navigation(); ?>



      </div>
    </div>


  </div>
</div>