<?php
$title             = get_sub_field('title');
$subtitle          = get_sub_field('subtitle');
$testimonials_list = get_sub_field('items');
?>


<section class="testimonials">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner">


          <span class="title">— <strong><?php echo $title ?></strong> —</span>
          <span class="subtitle"><?php echo $subtitle ?></span>


          <?php
          if ($testimonials_list):
            ?>

            <?php
            $iteration = 2;
            $row_start = '<div class="item"><div class="row">';
            $row_end   = '</div></div>';
            ?>


            <div class="testimonials_list testimonials_list_desktop">
              <?php foreach ($testimonials_list as $post): ?>
                <?php
                setup_postdata($post);


                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 's200', false);
                ?>

                <?php if ($iteration == 2) echo $row_start; ?>


                <div class="col-md-6">





                  <img src="<?php echo $image_url[0]; ?>" alt=""> 

                  <span class="m_title"><?php the_title(); ?> . <span><?php the_field('pet_name') ?></span></span>


                  <div class="content">
                    <?php the_content(); ?>

                  </div><!-- content -->
                </div><!-- col-md-6 -->

                <?php
                $iteration--;
                if ($iteration == 0)
                  $iteration = 2;
                if ($iteration == 2)
                  echo $row_end;
                ?>

              <?php endforeach; ?>

              <?php
              if ($iteration != 0 && $iteration != 4)
                echo $row_end;
              ?>
            </div>
            <?php wp_reset_postdata(); ?>




            <div class="testimonials_list testimonials_list_mobile">



              <?php foreach ($testimonials_list as $post): ?>
                <?php
                setup_postdata($post);


                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 's200', false);
                ?>

                <div class="row">

                  <div class="col-md-12">





                    <img src="<?php echo $image_url[0]; ?>" alt=""> 

                    <span class="m_title"><?php the_title(); ?> . <span><?php the_field('pet_name') ?></span></span>


                    <div class="content">
                      <?php the_content(); ?>

                    </div><!-- content -->
                  </div><!-- col-md-12 -->

                </div><!-- row -->

              <?php endforeach; ?>


            </div>
            <?php wp_reset_postdata(); ?>

          <?php endif; ?> 


        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->



</section>