<?php 
//use Roots\Sage\Extras;
?>

<div class="posts__meta">

  <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
  
 <?php // echo Extras\custom_taxonomies_terms_links(); ?>
  <?php echo 'by ' . get_the_author()  ?>
</div><!-- posts__meta -->
