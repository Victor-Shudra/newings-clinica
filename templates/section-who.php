<?php
$image           = get_sub_field('image');
$title           = get_sub_field('title');
$background_text = get_sub_field('background_text');
$intro           = get_sub_field('intro');
$text            = get_sub_field('text');
$more_link       = get_sub_field('more_link');
$more_link_ext   = get_sub_field('more_link_ext');
$label           = get_sub_field('label');
$is_ext          = get_sub_field('is_ext');
?>

<section class="who-we-are">
  <div class="inner clearfix">
    <?php if ($image): ?>
      <div class="img-container">
        <img src="<?php echo $image['sizes']['w423h415'] ?>" alt="">
      </div>
    <?php endif; ?>


    <div class="title-box">
      <?php if ($title): ?>
        <h2><?php echo $title ?></h2>
      <?php endif; ?>
      <?php if ($background_text): ?>
        <div class="bg-text"><?php echo $background_text ?></div>
      <?php endif; ?>
    </div>

    <?php if ($intro): ?>
      <span class="sub-title"><?php echo $intro ?></span>
    <?php endif; ?>
    <?php if ($text || $label): ?>

      <p><?php echo $text ?>


        <?php if ($is_ext): ?>
          <?php if ($more_link_ext): ?>

            <a class="read-more" 
               target="_blank"
               href="<?php echo $more_link_ext ?>"><?php echo $label ?></a>

          <?php endif; ?>

        <?php else: ?>
          <?php if ($more_link): ?>

            <a class="read-more" href="<?php echo $more_link ?>"><?php echo $label ?></a>
          <?php endif; ?>
        <?php endif; ?>
      </p>
    <?php endif; ?>
  </div>
</section>