<?php
$title    = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$content  = get_sub_field('content');

$form_shortcode = get_sub_field('form_shortcode');

$subscribe_label = get_sub_field('subscribe_label');
$subscribe_text  = get_sub_field('subscribe_text');
$image           = get_sub_field('image');
?>


<section class="appointments"  
         id="appointments"
         style="background-image: url(<?php echo $image['sizes']['h672'] ?>)">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner">


          <span class="title">— <strong><?php echo $title ?></strong> —</span>
          <span class="subtitle"><?php echo $subtitle ?></span>
          <span class="text"><?php echo $content ?></span>

          <div class="w_form">
            <?php if ($form_shortcode): ?>
              <?php echo do_shortcode($form_shortcode) ?>
            <?php endif; ?>
          </div><!-- w_form -->


          <div class="subscribe_form">
            <div class="inner_form_sb">

              <div class="row">
                <div class="col-md-6">
                  <span class="sb_label"><?php echo $subscribe_label ?></span>
                  <span class="sb_text"><?php echo $subscribe_text ?></span>


                </div><!-- col-md-6 -->
                <div class="col-md-6">
                  <?php
                  $subscribe_form_shortcode = get_sub_field('subscribe_form_shortcode');
                  ?>
                  <?php if ($subscribe_form_shortcode): ?>
                    <?php echo do_shortcode($subscribe_form_shortcode) ?>
                  <?php endif; ?>
                

                </div><!-- col-md-6 -->

              </div><!-- row -->

            </div><!-- inner_form_sb -->

          </div><!-- subscribe_form -->


        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->



</section>

