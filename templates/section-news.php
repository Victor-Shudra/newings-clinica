<?php
$image           = get_sub_field('image');
$description     = get_sub_field('description');
$link_label      = get_sub_field('link_label');
$title           = get_sub_field('title');
$background_text = get_sub_field('background_text');
?>

<section class="news-events">
  <div class="inner clearfix">


    <div class="left-news">
      <?php if ($image): ?>

        <div class="img-container">
          <img src="<?php echo $image['sizes']['w421h416'] ?>" alt="">
        </div>
      <?php endif; ?>
      <?php if ($description): ?>

        <span class="sub-title"><?php echo $description ?></span>
      <?php endif; ?>
      <?php if ($link_label): ?>

        <div class="btn-container">
          <a class="btn-link" href="<?php echo get_page_link(45) ?>"><?php echo $link_label ?></a>
        </div>			
      <?php endif; ?>
    </div><!-- left-news" -->


    <div class="right-news">


      <div class="title-box title-right">
        <?php if ($title): ?>

          <h2><?php echo $title ?></h2>
        <?php endif; ?>
        <?php if ($background_text): ?>

          <div class="bg-text"><?php echo $background_text ?></div>
        <?php endif; ?>
      </div>



      <?php
      $arg = array(
        'posts_per_page' => 3
      );

      $the_query = new WP_Query($arg);
      if ($the_query->have_posts()) :
        $i = 1;
        ?>

        <div class="news-events-block">
          <?php
          while ($the_query->have_posts()) : $the_query->the_post();

            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w226h143', false);

            $helper_class = '';
            if ($i > 1) {
              $helper_class = 'event' . $i;
            }
            ?>


            <div class="event <?php echo $helper_class ?>">
              <?php if ($image_url[0]): ?>

                <div class="img-container">
                  <span class="colored-border">
                    <img alt="" src="<?php echo $image_url[0]; ?>">
                  </span>
                </div>
              <?php endif; ?>
              <div class="event-content">
                <h3><?php the_title(); ?></h3>
                <span class="date">Posted: <?php echo get_post_time('M. d, Y'); ?></span>
                <?php the_excerpt(); ?>
                <a class="event-read-more" href="<?php the_permalink(); ?>"><?php _e('Read More', 'sage') ?></a>
              </div>
            </div>




            <?php
            $i++;
          endwhile;
          ?>


        </div><!-- news-events-block -->
        <?php
      endif;
      wp_reset_postdata();
      ?> 





    </div><!-- right-news -->


  </div><!-- inner -->
</section>