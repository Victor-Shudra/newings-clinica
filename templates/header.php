<?php
$logo     = get_field('logo', 'options');
$cta_text = get_field('cta_text', 'options');
?>

<header id="header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner">


          <a href="<?php bloginfo('url') ?>" class="logo">
            <img src="<?php echo $logo['sizes']['w150h120'] ?>" alt="" width="150" height="120">
          </a>

          <div class="header__cta">
            <?php echo $cta_text ?>
          </div><!-- header__cta -->

          <?php do_action('icl_language_selector'); ?>

          <nav class="navbar navbar-default">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed"
                      data-toggle="collapse"
                      data-target="#bs-example-navbar-collapse-0"
                      aria-expanded="false">
                <span class="sr-only"><?php _e('Toggle navigation', 'sage') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <div class="collapse navbar-collapse mob-menu" id="bs-example-navbar-collapse-0">

              <?php
              if (has_nav_menu('primary_navigation')) :
                wp_nav_menu([
                  'theme_location' => 'primary_navigation',
                  'menu_class' => 'top-navigation'
                ]);
              endif;
              ?>

            </div><!-- /.navbar-collapse -->
          </nav>



        </div><!-- inner -->




      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->
</header>
<div class="header_push">

</div><!-- header_push -->