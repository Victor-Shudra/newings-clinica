<article <?php post_class('preview_first'); ?>>
  <div class="row">
    <div class="col-md-4">
      <?php the_post_thumbnail('w355') ?>
    </div><!-- col-md-4 -->

    <div class="col-md-8">
      <header>
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php get_template_part('templates/entry-meta'); ?>
      </header>
      <div class="entry-summary">
        <?php the_excerpt(); ?>
        
        <a href="<?php the_permalink(); ?>" 
           class="loop_more"><?php _e('Read More', 'sage') ?></a>
      </div>
    </div><!-- col-md-8 -->
  </div><!-- row -->

</article>
