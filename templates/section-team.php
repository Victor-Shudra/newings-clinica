<?php
$title    = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$team     = get_sub_field('team');
?>


<section class="team" id="team">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner">


          <span class="title">— <strong><?php echo $title ?></strong> —</span>
          <span class="subtitle"><?php echo $subtitle ?></span>


          <?php
          if ($team):
            ?>
            <div class="team_members team_members_desktop">
              <?php foreach ($team as $post): ?>
                <?php
                setup_postdata($post);


                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w185h230', false);
                ?>



                <div class="item">

                  <div class="w_image">

                    <img src="<?php echo $image_url[0]; ?>" alt=""> 
                  </div><!-- w_image -->

                  <div class="m_descr">



                    <span class="m_title"><?php the_title(); ?></span>
                    <span class="m_position"><?php the_field('position'); ?></span>

                    <div class="content">
                      <?php the_content(); ?>
                    </div><!-- content -->
                  </div><!-- m_descr -->

                </div>
              <?php endforeach; ?>
            </div>
            <?php wp_reset_postdata(); ?>
          
          
          
          
           <div class="team_members team_members_mobile">
              <?php foreach ($team as $post): ?>
                <?php
                setup_postdata($post);


                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'w185h230', false);
                ?>



                <div class="item">

                  <div class="w_image">

                    <img src="<?php echo $image_url[0]; ?>" alt=""> 
                  </div><!-- w_image -->

                  <div class="m_descr">



                    <span class="m_title"><?php the_title(); ?></span>
                    <span class="m_position"><?php the_field('position'); ?></span>

                    <div class="content">
                      <?php the_content(); ?>
                    </div><!-- content -->
                  </div><!-- m_descr -->

                </div>
              <?php endforeach; ?>
            </div>
            <?php wp_reset_postdata(); ?>
          
          
          
          <?php endif; ?> 


        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->



</section>