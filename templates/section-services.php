<?php
$title    = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$services = get_sub_field('services');
?>


<section class="services" id="services">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner">


          <span class="title">— <strong><?php echo $title ?></strong> —</span>
          <span class="subtitle"><?php echo $subtitle ?></span>


          <?php
          if ($services):
            ?>
            <div class="service_list">
              <?php foreach ($services as $post): ?>
                <?php
                setup_postdata($post);


                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 's100', false);
                ?>



                <div class="item">
                  
                  <div class="item_inner">

                     


                  <img src="<?php echo $image_url[0]; ?>" alt=""> 


                  <span class="m_title"><?php the_title(); ?></span>

                  <div class="content">
                    <?php the_content(); ?>-
                    
                  </div><!-- content -->
                  
                  
                   </div><!-- item_inner -->
                </div><!-- item -->

              <?php endforeach; ?>
            </div>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?> 


        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->



</section>