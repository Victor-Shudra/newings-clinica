

<div class="col-md-4">
  <article <?php post_class(); ?>>
    <header>
      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('w355h150') ?></a>
      <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-summary">
      <?php the_excerpt(); ?>
      
       <a href="<?php the_permalink(); ?>" 
           class="loop_more"><?php _e('Read More', 'sage') ?></a>
    </div>
  </article>
</div><!-- col-md-4 -->
