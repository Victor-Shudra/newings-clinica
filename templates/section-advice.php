<?php
$title       = get_sub_field('title');
$subtitle    = get_sub_field('subtitle');
$advice_list = get_sub_field('items');
?>


<section class="advices" id="advice">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner">


          <span class="title">— <strong><?php echo $title ?></strong> —</span>
          <span class="subtitle"><?php echo $subtitle ?></span>


          <?php
          if ($advice_list):
            ?>
            <div class="advice_list">
              <?php foreach ($advice_list as $post): ?>
                <?php
                setup_postdata($post);


                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 's350', false);
                ?>



                <div class="item">


                  <div class="row">
                    <div class="col-md-5">
                      <img src="<?php echo $image_url[0]; ?>" alt=""> 
                    </div><!-- col-md-6 -->
                    <div class="col-md-7">

                      <div class="content">
                      <span class="m_title"><?php the_title(); ?></span>
                        <?php the_content(); ?>

                      </div><!-- content -->
                    </div><!-- col-md-6 -->
                  </div><!-- row -->


                </div><!-- item -->

              <?php endforeach; ?>
            </div>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?> 


        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->



</section>