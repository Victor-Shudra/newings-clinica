<div class="container">



  <div class="row row_page_inner">
    <div class="col-md-10 col-centered">



      <?php while (have_posts()) : the_post(); ?>
        <article <?php post_class(); ?>>

          <div class="entry-content content_single">
            <div class="row">



              <div class="col-md-4 col_thumb">
                <?php the_post_thumbnail('large') ?>
              </div><!-- col-md-4 -->

              <div class="col-md-8">
                <h1 class="entry-title"><a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?></a>
                </h1>

                <?php get_template_part('templates/entry-meta'); ?>


                <?php the_content(); ?>
              </div><!-- col-md-8 -->


            </div><!-- row -->


            <!-- SIMILAR -->

            <?php
            $arg = array(
              'orderby' => 'rand',
              'post__not_in' => array(get_the_ID()),
              'posts_per_page' => 3
            );

            $the_query = new \WP_Query($arg);
            if ($the_query->have_posts()) :
              ?>
              <div class="row row_more-posts">
                <?php
                while ($the_query->have_posts()) : $the_query->the_post();
                  ?>
                  <?php get_template_part('templates/content', 'loop'); ?>
                <?php endwhile; ?>
              </div><!-- row -->
              <?php
            endif;
            wp_reset_postdata();
            ?> 




          </div>


        </article>
      <?php endwhile; ?>


    </div><!-- col-md-12 -->
  </div><!-- row -->

</div><!-- container -->