<?php
$title    = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$images   = get_sub_field('images');
$content  = get_sub_field('content');
?>


<section class="about" id="clinic">
  <div class="inner">




    <span class="title">— <strong><?php echo $title ?></strong> —</span>
    <span class="subtitle"><?php echo $subtitle ?></span>


    <div class="images">


      <?php
      if ($images):
        $i = 1;
        ?>
        <?php foreach ($images as $image): ?>

          <?php if ($i == 1): ?>
            <div class="col col_1">
              <img src="<?php echo $image['sizes']['w421']; ?>" alt="">
            </div><!-- col -->
          <?php endif; ?>

          <?php if ($i == 2): ?>
            <div class="col col_2">
              <img src="<?php echo $image['sizes']['w421']; ?>" alt="">
            <?php endif; ?>


            <?php if ($i == 3): ?>
              <img src="<?php echo $image['sizes']['w421']; ?>" alt="">
            </div><!-- col -->
          <?php endif; ?>





          <?php if ($i == 4): ?>
            <div class="col col_3">
              <img src="<?php echo $image['sizes']['w421']; ?>" alt="">

              <?php echo $content ?>
            </div><!-- col -->
          <?php endif; ?>


          <?php if ($i == 5): ?>
            <div class="col col_4">
              <img src="<?php echo $image['sizes']['w421']; ?>" alt="">
            </div><!-- col -->
          <?php endif; ?>




          <?php
          $i++;
        endforeach;
        ?>
      <?php endif;
      ?>  
    </div><!-- images -->





  </div><!-- inner -->




</section>