<?php
$image        = get_sub_field('image');
$image_tablet = get_sub_field('image_tablet');
$title        = get_sub_field('title');
$subtitle     = get_sub_field('subtitle');
$button_label = get_sub_field('button_label');
?>

<section class="banner banner_desktop"
         style="background-image: url(<?php echo $image['sizes']['h672'] ?>)"
         >

  <div class="container">
    <div class="row static">
      <div class="col-md-12 static">
        <div class="inner">

          <div class="content">


            <h1><?php echo $title ?></h1>

            <h2><?php echo $subtitle ?></h2>


            <a href="#appointments" class="btn_banner"><?php echo $button_label ?></a>
          </div><!-- content -->

        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->

</section>

<section class="banner banner_tablet"
         style="background-image: url(<?php echo $image_tablet['sizes']['h672'] ?>)"
         >

  <div class="container">
    <div class="row static">
      <div class="col-md-12 static">
        <div class="inner">

          <div class="content">


            <h1><?php echo $title ?></h1>

            <h2><?php echo $subtitle ?></h2>


            <a href="#appointments" class="btn_banner"><?php echo $button_label ?></a>
          </div><!-- content -->

        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->

</section>