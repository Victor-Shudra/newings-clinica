<?php
$logo_f       = get_field('logo_f', 'options');
$image_f      = get_field('image_f', 'options');
$image_text_f = get_field('image_text_f', 'options');
$cta_text_f   = get_field('cta_text_f', 'options');
$text_area    = get_field('text_area', 'options');
?>

<footer id="footer">

  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-3">
        <?php if ($logo_f): ?>

          <a href="<?php bloginfo('url') ?>">
            <img
              class="logo_f"
              src="<?php echo $logo_f['sizes']['w150h120'] ?>" alt="" width="150" height="120">
          </a>
        <?php endif; ?>
      </div><!-- col-md-3 -->


      <div class="col-md-9 col-sm-9  col-xs-9">
        <div class="inner">

          <?php
          if (has_nav_menu('footer_navigation')) :
            wp_nav_menu([
              'theme_location' => 'footer_navigation',
              'menu_class' => 'footer-navigation'
            ]);
          endif;
          ?>

        </div><!-- inner -->

        <div class="row row_details">
          <div class="col-md-4">
            <div class="footer_cta footer_cta_tablet">
              <?php echo $cta_text_f ?>


             

            </div><!-- footer_cta -->

            <div class="footer__text_1">
              <?php if ($image_f): ?>
                <img src="<?php echo $image_f['url'] ?>" alt="">
              <?php endif; ?>
              <?php echo $image_text_f ?>


            </div><!-- footer__text_1 -->
          </div><!-- col-md-4 -->

          <div class="col-md-8">

            <div class="footer_cta footer_cta_desktop">
              <?php echo $cta_text_f ?>

<span class="footer_add_link footer_add_link_desktop">
              <?php
              $policy_link_file = get_field('policy_link_file', 'options');
              $policy_link_text = get_field('policy_link_text', 'options');
              ?>
              <?php if ($policy_link_file && $policy_link_text): ?>
                <a href="<?php echo $policy_link_file['url'] ?>" 
                   target="_blank"
                   class="policy-link-desktop"><?php echo $policy_link_text ?></a>
                 <?php endif; ?>

                  | <a href="<?php the_field('additional_link_f', 'options') ?>" target="_blank"><?php the_field('additional_link_text_f', 'options') ?></a>
                  </span>

            </div><!-- footer_cta -->

          </div><!-- col-md-9 -->

        </div><!-- row -->




      </div><!-- col-md-9 -->

    </div><!-- row -->


    <div class="row">
      <div class="col-md-12">
        <div class="bottom_text_f">
          <?php echo $text_area ?>
          
          <span class="footer_add_link footer_add_link_tablet">
           <?php
              $policy_link_file = get_field('policy_link_file', 'options');
              $policy_link_text = get_field('policy_link_text', 'options');
              ?>
              <?php if ($policy_link_file && $policy_link_text): ?>
                <a href="<?php echo $policy_link_file['url'] ?>" 
                   target="_blank"
                   class="policy-link-tablet"><?php echo $policy_link_text ?></a>
                 <?php endif; ?>
                 
                  | <a href="<?php the_field('additional_link_f', 'options') ?>" target="_blank"><?php the_field('additional_link_text_f', 'options') ?></a>
                  </span>
        </div><!-- bottom_text_f -->
      </div><!-- col-md-12 -->
    </div><!-- row -->
  </div><!-- container -->



  <div class="footer__bottom">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col_l">
          <p>© <?php bloginfo('name') ?> <?php echo date('Y') ?></p>
        </div><!-- col-md-6 -->
        <div class="col-sm-6 col_r">
          <a target="_blank" href="http://www.newings-design.com/">Design by Newings</a>
        </div><!-- col-md-6 -->
      </div><!-- row -->
    </div><!-- container -->
  </div><!-- footer__bottom -->

</footer>

