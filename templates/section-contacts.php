<?php
$title    = get_sub_field('title');
$subtitle = get_sub_field('subtitle');

$phone_text    = get_sub_field('phone_text');
$facebook_text = get_sub_field('facebook_text');
$facebook_link = get_sub_field('facebook_link');
$address_text  = get_sub_field('address_text');
$email_text    = get_sub_field('email_text');
$email         = get_sub_field('email');

$icon_1 = get_sub_field('icon_1');
$icon_2 = get_sub_field('icon_2');
$icon_3 = get_sub_field('icon_3');
$icon_4 = get_sub_field('icon_4');
?>


<section class="contacts" 
         id="contacts"
         style="background-image: url(<?php echo $image['sizes']['h672'] ?>)">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="inner">


          <span class="title">— <strong><?php echo $title ?></strong> —</span>
          <span class="subtitle"><?php echo $subtitle ?></span>


          <div class="row row_contacts">


            <div class="col-md-3 col-sm-6">
              <div class="w_img">
                <?php if ($icon_1): ?>
                  <img src="<?php echo $icon_1['url'] ?>" alt="">
                <?php endif; ?>
              </div><!-- w_img -->
              -
              <div class="text">
                <?php echo $phone_text ?>
              </div><!-- text -->
              -
            </div><!-- col-md-3 col-sm-6 -->


            <div class="col-md-3 col-sm-6">
              <div class="w_img">

                    <?php if ($icon_2): ?>
                  <img src="<?php echo $icon_2['url'] ?>" alt="">
                <?php endif; ?>
              </div><!-- w_img -->
              -
              <div class="text">
                <a href="<?php echo $facebook_link ?>" target="_blank"><?php echo $facebook_text ?></a>
              </div><!-- text -->
              -
            </div><!-- col-md-3 col-sm-6 -->


            <div class="col-md-3 col-sm-6">
              <div class="w_img">

                   <?php if ($icon_3): ?>
                  <img src="<?php echo $icon_3['url'] ?>" alt="">
                <?php endif; ?>
              </div><!-- w_img -->
              -
              <div class="text">
                <?php echo $address_text ?>
              </div><!-- text -->
              -
            </div><!-- col-md-3 col-sm-6 -->


            <div class="col-md-3 col-sm-6">
              <div class="w_img">

                    <?php if ($icon_4): ?>
                  <img src="<?php echo $icon_4['url'] ?>" alt="">
                <?php endif; ?>
              </div><!-- w_img -->
              -
              <div class="text">
                <a href="mailto:<?php $email ?>"><?php echo $email_text ?></a>
              </div><!-- text -->
              -
            </div><!-- col-md-3 col-sm-6 -->


          </div><!-- row -->



          <div class="sb__facebook">
            <?php echo do_shortcode('[custom-facebook-feed]') ?>
          </div><!-- sb__facebook -->

          <div class="w_map">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3185.531561009874!2d-7.927130684273392!3d37.02097026294003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd0552dabe00582b%3A0x829583a6ab5b5cb9!2sCl%C3%ADnica+Veterin%C3%A1ria+de+Faro!5e0!3m2!1sen!2sua!4v1485505661902" width="600" height="405" frameborder="0" style="border:0" allowfullscreen></iframe>

          </div><!-- w_map -->
        </div><!-- inner -->

      </div><!-- col-md-12 -->

    </div><!-- row -->

  </div><!-- container -->



</section>

