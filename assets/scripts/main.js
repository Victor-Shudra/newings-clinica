/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */



(function ($) {



  // setInterval X times
  function setIntervalX(callback, delay, repetitions) {
    var x = 0;
    var intervalID = window.setInterval(function () {

      callback();

      if (++x === repetitions) {
        window.clearInterval(intervalID);
      }
    }, delay);
  }




  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function () {
        
        
        $(window).scroll(function() {
          if($(this).scrollTop() > 0 ) {
            $('body').addClass('scrolled');
          } else {
            $('body').removeClass('scrolled');
          }
        });
        

        $('.service_list').slick({
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 3,
          dots: false,
          autoplay: true,
          autoplaySpeed: 10000,
          speed: 300,
          prevArrow: '<span class="prev slick_ar"></span>',
          nextArrow: '<span class="next slick_ar"></span>',
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });


//        var slider_settings = {
//          infinite: true,
//          slidesToShow: 1,
//          slidesToScroll: 1,
//          dots: false,
//          autoplay: true,
//          autoplaySpeed: 6000,
//          speed: 300,
//          fade: true,
//          prevArrow: '<span class="prev slick_ar"></span>',
//          nextArrow: '<span class="next slick_ar"></span>'
//        };

//        if ($(window).width() < 768) {
//          $('.team_members').slick(slider_settings);
//        }
//
//
//        $(window).on('resize', function () {
//          if ($(window).width() < 768) {
//            $('.team_members').slick(slider_settings);
//          } else {
//            $('.team_members').slick('unslick');
//          }
//        });





        $('.team_members_mobile').slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          autoplay: true,
          autoplaySpeed: 6000,
          speed: 300,
          fade: true,
          prevArrow: '<span class="prev slick_ar"></span>',
          nextArrow: '<span class="next slick_ar"></span>'

        });




        $('.advice_list').slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          autoplay: true,
          autoplaySpeed: 6000,
          speed: 300,
          fade: true,
          prevArrow: '<span class="prev slick_ar"></span>',
          nextArrow: '<span class="next slick_ar"></span>',
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });


        $('.testimonials_list').slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          autoplay: true,
          autoplaySpeed: 6000,
          speed: 600,
          fade: true,
          prevArrow: '<span class="prev slick_ar"></span>',
          nextArrow: '<span class="next slick_ar"></span>',
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });




        $('.navbar-header button').on('click', function () {
          if ($('.navbar-default').hasClass('opened')) {
            $('.navbar-default').removeClass('opened');
          } else {
            $('.navbar-default').addClass('opened');
          }
        });

        $('a[href*="#"]').click(function (e) {
          e.preventDefault();
          var target = $(this).attr('href');
          if ($(target).length) {
            if ($(window).width() < 768) {
              $('.navbar-header [aria-expanded="true"]').click();
            }


            $('html,body').animate({
              scrollTop: $(target).offset().top - $('#header').height() - $('#wpadminbar').height()
            }, 1000);
            return false;
          }
        });

      },
      finalize: function () {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Contact page
    'contact': {
      init: function () {

      },
      finalize: function () {

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
        UTIL.fire(classnm);
//        console.log(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
