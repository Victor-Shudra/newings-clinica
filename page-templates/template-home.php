<?php

/**
 * Template Name: Home
 */
?>

<?php

if (have_rows('sections')):
  while (have_rows('sections')) : the_row();

    if (get_row_layout() == 'banner'):
      get_template_part('templates/section', 'banner');

   
    elseif (get_row_layout() == 'team'):
      get_template_part('templates/section', 'team');

   
    elseif (get_row_layout() == 'about'):
      get_template_part('templates/section', 'about');

   
    elseif (get_row_layout() == 'services'):
      get_template_part('templates/section', 'services');

   
    elseif (get_row_layout() == 'advices'):
      get_template_part('templates/section', 'advice');
   
    
    elseif (get_row_layout() == 'testimonials'):
      get_template_part('templates/section', 'testimonials');
   
    
    elseif (get_row_layout() == 'appointments'):
      get_template_part('templates/section', 'appointments');
   
    
    elseif (get_row_layout() == 'contacts'):
      get_template_part('templates/section', 'contacts');


    endif;

  endwhile;

else :


endif;
?>  

