<?php
/*
  Template Name: Blog
  Template Post Type: page
 */

use Roots\Sage\Extras;

global $more;
$more  = 0;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>
<div class="body-inner">
  <div class="container">


    <div class="bc">
      <div class="row">
        <div class="col-md-12">
          <?php Extras\breadcrumb_trail('echo=1&separator=|'); ?>
        </div><!-- col-md-12 -->
      </div><!-- row -->
    </div><!-- bc -->


    <div class="row">
      <div class="col-md-12">

        <div class="header-intro header-intro_sub">


          <h1><?php the_title(); ?></h1>


        </div><!-- header-intro -->
      </div>
    </div><!-- row -->

    <div class="row row_page_inner posts-list posts-list_blog">
      <div class="col-md-12">



        <?php query_posts('post_type=post&paged=' . $paged); ?>
        <?php
        if (have_posts()) :
          $i = 1;

          $iteration = 3;
          $row_start = '<div class="row">';
          $row_end   = '</div>';
          ?>

          <?php while (have_posts()) : the_post(); ?>
            <?php
            if ($i == 1):
              ?>

              <?php get_template_part('templates/content', 'post-first'); ?>




            <?php else:
              ?>
              <?php if ($iteration == 3) echo $row_start; ?>

              <?php get_template_part('templates/content', 'loop'); ?>

              <?php
              $iteration--;
              if ($iteration == 0)
                $iteration = 3;
              if ($iteration == 3)
                echo $row_end;
              ?>
            <?php endif; ?>




            <?php
            $i++;
            ?>



          <?php endwhile; ?>

          <?php if ($i > 2): ?>
            <?php
            if ($iteration != 0 && $iteration != 3) {
              echo $row_end;
            }
            ?>
          <?php endif; ?>

          <?php get_template_part('templates/pagination', 'post'); ?>

        <?php else : ?>
          <?php get_template_part('templates/content', 'none'); ?>
        <?php
        endif;
        wp_reset_query();
        ?>

      </div><!-- col-md-10  -->

    </div><!-- row -->



  </div><!-- container -->
</div><!-- body -->


</div>
</main>


