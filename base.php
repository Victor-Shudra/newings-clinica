<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <?php
    do_action('get_header');
    get_template_part('templates/header');
    ?>
  
    <?php include Wrapper\template_path(); ?>
   
    <?php
    do_action('get_footer');
    get_template_part('templates/footer');
    wp_footer();
    ?>
    <script type="text/javascript">
 jQuery( document ).ready(function() {
      document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '339' == event.detail.contactFormId ) {
        jQuery('.appointments .subscribe_form').addClass('sent');
    }
    if ( '4' == event.detail.contactFormId ) {
        jQuery('.appointments .w_form').addClass('sent');
    }
    if ( '314' == event.detail.contactFormId ) {
        jQuery('.appointments .w_form').addClass('sent');
    }
    if ( '337' == event.detail.contactFormId ) {
        jQuery('.appointments .subscribe_form').addClass('sent');
    }
}, false );


jQuery('a[href*="#"]:not([href="#"])').click(function () {
        var target = jQuery(this.hash);
        if (target.length) {
          jQuery('html,body').animate({
            scrollTop: target.offset().top - jQuery("#header").height()
          }, 400);
          return false;
        }
    });
    });
</script>
      </body>
</html>
